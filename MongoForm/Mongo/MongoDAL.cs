﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoForm.Mongo
{
    public class MongoDAL
    {
        private readonly MongoClient client;
        private readonly IMongoDatabase db;
        public MongoDAL()
        {
            string mongoConnection = ConfigurationManager.AppSettings["MongoDBConnection"];
            string dbName = ConfigurationManager.AppSettings["MongoDBName"];
            client = new MongoClient(mongoConnection);
            db = client.GetDatabase(dbName);
        }

        public bool InsertDocument<T>(string Collection, T Record)
        {
            try
            {
                var collection = db.GetCollection<T>(Collection);
                collection.InsertOne(Record);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<T> GetCollection<T>(string Collection)
        {
            return db.GetCollection<T>(Collection).AsQueryable().ToList();
            //return db.GetCollection<T>(Collection).Find(new BsonDocument()).ToList();
        } 

        public T GetDocumentById<T>(string Collection, ObjectId Id)
        {
            var collection = db.GetCollection<T>(Collection);

            var filter = Builders<T>.Filter.Eq("_id", Id);

            return collection.Find(filter).First();
        }

        public void UpsertDocument<T>(string Collection, ObjectId id, T document)
        {
            var collection = db.GetCollection<T>(Collection);

            var result = collection.ReplaceOne(
                new BsonDocument("_id", id),
                document,
                new UpdateOptions { IsUpsert = true });

        }

        public void DeleteDocument<T>(string Collection, ObjectId id)
        {
            var collection = db.GetCollection<T>(Collection);
            var filter = Builders<T>.Filter.Eq("_id", id);
            collection.DeleteOne(filter);
        }

    }
}
