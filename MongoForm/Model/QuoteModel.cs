﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoForm.Model
{
    public class QuoteModel
    {
        public string Content { get; set; }
        public string Author { get; set; }
        public DateTime CreatedOn { get; set; }

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("updated_on")]
        public DateTime UpdatedOn { get; set; }
    }
}
