﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoForm.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver.Linq;
using System.Configuration;
using MongoForm.Mongo;

namespace MongoForm
{
    public partial class Home : Form
    {
        private MongoDAL mongo;
        private ObjectId quoteSelected;
        public Home()
        {
            InitializeComponent();
            mongo = new MongoDAL();
            RefreshGrid();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            NewDocument();
            RefreshGrid();
        }

        private void NewDocument()
        {
            var model = new QuoteModel
            {
                Id = ObjectId.GenerateNewId(),
                Content = txtQuote.Text,
                Author = txtAuthor.Text,
                CreatedOn = DateTime.Now
            };

            mongo.InsertDocument("quote", model);
            MessageBox.Show("Insert ducument successful");
        }
        private void RefreshGrid()
        {
            quoteGrid.DataSource = mongo.GetCollection<QuoteModel>("quote");
        }

        private void quoteGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            QuoteModel result = (QuoteModel)quoteGrid.Rows[e.RowIndex].DataBoundItem;
            txtAuthor.Text = result.Author;
            txtQuote.Text = result.Content;
            quoteSelected = result.Id;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var model = new QuoteModel
            {
                Content = txtQuote.Text,
                Author = txtAuthor.Text,
                CreatedOn = DateTime.Now
            };

            var doc = mongo.GetDocumentById<QuoteModel>("quote", quoteSelected);
            doc.UpdatedOn = DateTime.Now;
            doc.Content = txtQuote.Text;
            doc.Author = txtAuthor.Text;
            mongo.UpsertDocument("quote", doc.Id, doc);
            RefreshGrid();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            mongo.DeleteDocument<QuoteModel>("quote", quoteSelected);
            RefreshGrid();
        }
    }
}
